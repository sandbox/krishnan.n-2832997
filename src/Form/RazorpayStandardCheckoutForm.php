<?php

namespace Drupal\razorpay\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\DataCommand;


// require('lib/razorpay-php/Razorpay.php');
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

/**
 * Class RazorpayStandardCheckoutForm.
 */
class RazorpayStandardCheckoutForm extends FormBase {

  private $keyId, $keySecret;
  private $rpApi;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'razorpay_standard_checkout_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['productname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ProductName'),
      '#description' => $this->t('Name of Product'),
      '#weight' => '0',
    ];
    // TODO: The Currency Symbol is now 'XXX', should be changed to INR.
    // Not sure how/were to add the currency symbol value 'INR'
    $form['price'] = [
      '#type' => 'currency_amount',
      '#title' => $this->t('Price'),
      '#description' => $this->t('Cost of item in Rupees * 100 (paisa)'),
      '#weight' => '1',
    ];

    // -- nk HACK. Trying to add <script src=...> into the <form> </form>
    $verify_url = \Drupal\Core\Url::fromRoute('razorpay.rp_standrd_checkout_verify_controller_RpStandardCheckoutVerify');
    $form['#action'] = $verify_url->toString();

    /* TODO: Add the parameters to "src=checkout.js part */

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Checkout'),
      '#weight' => '2',
      '#attributes' => ['class' =>  ['razorpay-payment-button']],
    ];

    $form['#attached']['drupalSettings']['razorpay_stdpay'] = "Here";
    $data = $this->RpCreateOrderDetails($form, $form_state);

    return $form;
  }



  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }


  private function RpCreateOrderDetails(array &$form, FormStateInterface $form_state)  {

    $rpcfg = \Drupal::config('razorpay.razorpaykeys');
    $keyId = $rpcfg->get('razorpay_key_id');
    $keySecret = $rpcfg->get('key_secret');

    // drupal_set_message('key values from config: ' . $keyId . ", " . $keySecret);
    // drupal_set_message('validateForm, product name : '
    //   . $form_state->getValue('productname'));
    $arr = $form_state->getValue('price');

    foreach ($arr as $k => $v)
      drupal_set_message("Key: " . $k . ", Value: " . $v);

    $rpApi = new Api($keyId, $keySecret);
    // TODO: Trap error

    if (! isset ($arr['amount']))
      $arr['amount'] = 1024;

    $orderData = [
      'receipt'  => 1111,
      'amount'   => $arr['amount']  * 100,
      'currency'        => 'INR',
      'payment_capture' => 1 // auto capture
    ];


    $razorpayOrder = $rpApi->order->create($orderData);

    $razorpayOrderId = $razorpayOrder['id'];
    $_SESSION['razorpay_order_id'] = $razorpayOrderId;
    $displayAmount = $amount = $orderData['amount'];
    $displayAmount = $amount = $orderData['amount'];

    $data = [
        "key"               => $keyId,
        "amount"            => $amount,
        "name"              => $form_state->getValue('productname'),
        "description"       => 'description Not available',
        "image"             => 'image NA',
        "prefill"           => [
        "name"              => $form_state->getValue('productname'),
        "email"             => "test@example.com",
        "contact"           => "9999999999",
        ],
        "notes"             => [
        "address"           => "Addr 1",
        "merchant_order_id" => "12312321",
        ],
        "theme"             => [
        "color"             => "#F37254"
        ],
        "order_id"          => $razorpayOrderId,
    ];

    $data['display_currency']  = 'INR';
    $data['display_amount']    = $displayAmount;

    $form['#attached']['library'][] = 'razorpay/razorpay_checkoutjs';
    $form['#attached']['drupalSettings']['razorpay_stdpay'] = array(
      "key" => $keyId,
      "amount" => $amount,
      "currency" => "INR",
      "name" => "Dummy Name",
      "image" => "https://s29.postimg.org/r6dj1g85z/daft_punk.jpg",
      "description" =>  "Dummy description ",
      "prefill_name" =>  "Dummy prefill name ",
      "prefill_email" =>  "nobody@example.com",
      "prefill_contact" =>  "9999999999",
      "notes_shopping_order_id" =>  "3456",
      "order_id" => $razorpayOrderId,
    );

    // return array ('amount' => 1024.00, 'name' => 'product1');
    return $data;
    // return $orderData;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Bug: RpCreateOrderDetails should ideally be called here, but 
    // js/standardpayment.js is called from loadForm(), not here. 
    // $data = $this->RpCreateOrderDetails($form, $form_state);
    // Display result.
    // foreach ($form_state->getValues() as $key => $value) {
      // $rendered_message = \Drupal\Core\Render\Markup::create('<pre>' . $key . ': ' . print_r($value, true) . '</pre>');
      // drupal_set_message($rendered_message);
      // drupal_set_message('submitForm: ' . $key . ': ' . $value);
    // }
  }
} // Class end.
