<?php

namespace Drupal\razorpay\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RazorpayKeys.
 */
class RazorpayKeys extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'razorpay.razorpaykeys',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'razorpay_keys';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('razorpay.razorpaykeys');
    $form['razorpay_key_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Razorpay key id'),
      '#description' => $this->t('Key ID'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('razorpay_key_id'),
    ];
    $form['key_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key Secret'),
      '#description' => $this->t('Key Secret'),
      '#default_value' => $config->get('key_secret'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('razorpay.razorpaykeys')
      ->set('razorpay_key_id', $form_state->getValue('razorpay_key_id'))
      ->set('key_secret', $form_state->getValue('key_secret'))
      ->save();
  }

}
