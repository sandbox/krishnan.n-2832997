<?php

namespace Drupal\razorpay\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RazorpayCheckoutOption.
 */
class RazorpayCheckoutOption extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'razorpay.razorpaycheckoutoption',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'razorpay_checkout_option';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('razorpay.razorpaycheckoutoption');
    $form['razorpaykey'] = [
      '#type' => 'radios',
      '#title' => $this->t('RazorpayKey'),
      '#description' => $this->t('Use Standard checkout if razorpay default checkout menu will do, use Manual if your application needs special form elements'),
      '#options' => ['Standard Checkout' => $this->t('Standard Checkout'), 'Manual Checkout' => $this->t('Manual Checkout')],
      '#default_value' => $config->get('razorpaykey'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('razorpay.razorpaycheckoutoption')
      ->set('razorpaykey', $form_state->getValue('razorpaykey'))
      ->save();
  }

}
