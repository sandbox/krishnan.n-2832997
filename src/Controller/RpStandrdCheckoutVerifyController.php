<?php

namespace Drupal\razorpay\Controller;

use Drupal\Core\Controller\ControllerBase;
// -- nk. For Verify stuff
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;




/**
 * Class RpStandrdCheckoutVerifyController.
 */
class RpStandrdCheckoutVerifyController extends ControllerBase {

  private function RpVerifyLiftedCode()  {
    // require('razorpay-php/Razorpay.php');

    // -- nk. From config.php.
    /* $keyId = 'rzp_test_KNOE8m3V2CgMHn,gJSRRDfr8a4KZ9vB5aerykHC';
    $keySecret = 'rzp_test_BcLUDCb0VGXLMj,8KedbMnKr7qVPBibmo1yt7DG';
     */
    $displayCurrency = 'INR';
    $rpcfg = \Drupal::config('razorpay.razorpaykeys');
    $keyId = $rpcfg->get('razorpay_key_id');
    $keySecret = $rpcfg->get('key_secret');

    $success = true;

    $error = "Payment Failed";

    if (empty($_POST['razorpay_payment_id']) === false)
    {
        $api = new Api($keyId, $keySecret);

        try
        {
            // Please note that the razorpay order ID must
            // come from a trusted source (session here, but
            // could be database or something else)
            $attributes = array(
                'razorpay_order_id' => $_SESSION['razorpay_order_id'],
                'razorpay_payment_id' => $_POST['razorpay_payment_id'],
                'razorpay_signature' => $_POST['razorpay_signature']
            );

            $api->utility->verifyPaymentSignature($attributes);
        }
        catch(SignatureVerificationError $e)
        {
            $success = false;
            $error = 'Razorpay Error : ' . $e->getMessage();
        }
    }

    if ($success === true)
    {
        $html = "<p>RP Your payment was successful</p>
                 <p>Payment ID: {$_POST['razorpay_payment_id']}</p>";
    }
    else
    {
        $html = "<p>RP Your payment failed</p>
                 <p>{$error}</p>";
    }

    drupal_set_message ('Razorpay Verify. Will dump $api later');
    return $html;
  }



  /**
   * Rpstandardcheckoutverify.
   *
   * @return string
   *   Return Hello string.
   */
  public function RpStandardCheckoutVerify() {

    $ret =  $this->RpVerifyLiftedCode();
    return array(
        '#markup' => $ret,
      );


    /* return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: RpStandardCheckoutVerify')
    ]; */
  }
}
