(function ($, drupalSettings) {

  'use strict';
  console.log (drupalSettings.razorpay_stdpay)
  alert (drupalSettings.razorpay_stdpay)
  
  // Create script tag and add the data required
  var scriptTag = document.createElement("script");
  scriptTag.setAttribute("src", "https://checkout.razorpay.com/v1/checkout.js")
  scriptTag.setAttribute("data-key", drupalSettings.razorpay_stdpay.key)
  scriptTag.setAttribute("data-amount", drupalSettings.razorpay_stdpay.amount)
  scriptTag.setAttribute("data-currency", drupalSettings.razorpay_stdpay.currency)
  scriptTag.setAttribute("data-name", drupalSettings.razorpay_stdpay.name)
  scriptTag.setAttribute("data-image", drupalSettings.razorpay_stdpay.image)
  scriptTag.setAttribute("data-description", drupalSettings.razorpay_stdpay.description)
  scriptTag.setAttribute("data-notes.shopping_order_id", drupalSettings.razorpay_stdpay.notes_shopping_order_id)
  scriptTag.setAttribute("data-prefill.name", drupalSettings.razorpay_stdpay.prefill_name)
  scriptTag.setAttribute("data-prefill.email", drupalSettings.razorpay_stdpay.prefill_email)
  scriptTag.setAttribute("data-prefill.contact", drupalSettings.razorpay_stdpay.prefill_contact)
  scriptTag.setAttribute("data-notes.shopping_order_id", drupalSettings.razorpay_stdpay.notes_shopping_order_id)
  scriptTag.setAttribute("data-order_id", drupalSettings.razorpay_stdpay.order_id)
  
  // Attach the script to the form
  var frm = document.getElementById('razorpay-standard-checkout-form')
  frm.appendChild(scriptTag)
})(jQuery, drupalSettings);


// Study
// https://drupal.stackexchange.com/questions/250512/how-do-i-invoke-javascript-in-ajaxresponse
// https://www.drupal.org/docs/8/api/ajax-api/core-ajax-callback-commands
// https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21AjaxResponse.php/class/AjaxResponse/8.2.x
